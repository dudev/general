<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace general\assets;
use yii\web\AssetBundle;

/**
 * This asset bundle provides the [jquery javascript library](http://jquery.com/)
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class JVectorMapAsset extends AssetBundle
{
    public $sourcePath = '@general/distr/plugins/jvectormap';
	public $css = [
		'jquery-jvectormap-1.2.2.css',
	];
    public $js = [
        'jquery-jvectormap-1.2.2.min.js',
	    'jquery-jvectormap-world-mill-en.js',
    ];
	public $depends = [
		'yii\web\JqueryAsset',
	];
}
