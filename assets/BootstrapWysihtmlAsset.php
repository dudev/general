<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace general\assets;
use yii\web\AssetBundle;

/**
 * This asset bundle provides the [jquery javascript library](http://jquery.com/)
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BootstrapWysihtmlAsset extends AssetBundle
{
    public $sourcePath = '@general/distr/plugins/bootstrap-wysihtml5';
	public $css = [
		'bootstrap3-wysihtml5.css',
	];
    public $js = [
        'bootstrap3-wysihtml5.js',
    ];
	public $depends = [
		'yii\bootstrap\BootstrapPluginAsset',
	];
}
