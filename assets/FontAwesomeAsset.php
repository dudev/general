<?php
namespace general\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FontAwesomeAsset extends AssetBundle
{
	public $sourcePath = '@general/distr/font-awesome-4.5.0';
	public $css = [
		'css/font-awesome.css',
	];
}
