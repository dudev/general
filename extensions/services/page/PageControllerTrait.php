<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext\services\page;


use general\ext\api\page\PageApi;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

trait PageControllerTrait {
	public function actionView($nick) {
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}
		$data = PageApi::pageViewByNick(static::$domain, $nick);
		if($data['result'] == 'success') {
			if(!is_null($data['page']['template'])) {
				$this->layout = 'emptyHtml';
			}
			return $this->render('view', [
				'data' => $data['page'],
			]);
		}
		throw new NotFoundHttpException();
	}
}