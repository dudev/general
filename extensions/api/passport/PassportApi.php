<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext\api\passport;


use general\ext\api\BaseApi;

class PassportApi extends BaseApi {
	private static $service = 'passport';
	public static function passportSearch($search = [], $sort = '') {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($search as $k => $v) {
			$post['search[' . $k . ']'] = $v;
		}

		return self::execute(self::$service, 'passport/search',
			[
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'sort' => $sort,
			],
			$post
		);
	}
	public static function passportSearchById($ids = [], $fields = []) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		return self::execute(self::$service, 'passport/search-by-id',
			[
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'ids' => (array)$ids,
				'fields' => (array)$fields,
			]
		);
	}
	public static function passportCreate(array $Passport) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($Passport as $k => $v) {
			$post['Passport[' . $k . ']'] = $v;
		}

		return self::execute(self::$service,
			'passport/create', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
			],
			$post
		);
	}
	public static function passportUpdate($id, array $Passport = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		if($Passport === []) {
			return self::execute(self::$service,
				'passport/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'id' => $id,
				]
			);
		} else {
			$post = [];
			foreach ($Passport as $k => $v) {
				$post['Passport[' . $k . ']'] = $v;
			}

			return self::execute(self::$service,
				'passport/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'id' => $id,
				],
				$post
			);
		}
	}
	public static function passportDelete($id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'passport/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'id' => $id,
		]);
	}
}