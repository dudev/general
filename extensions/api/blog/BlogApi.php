<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext\api\blog;


use general\ext\api\BaseApi;

class BlogApi extends BaseApi {
	private static $service = 'blog';
	public static function postList($domain, $page = 0, $tag = '') {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		if($tag) {
			return self::execute(self::$service, 'post/list', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
				'page' => $page,
				'tag' => $tag,
			]);
		} else {
			return self::execute(self::$service, 'post/list', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
				'page' => $page,
			]);
		}
	}
	public static function postAdmin($domain, $page = 0, array $PostSearch = [], $sort = '') {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($PostSearch as $k => $v) {
			$post['PostSearch[' . $k . ']'] = $v;
		}

		return self::execute(self::$service, 'post/admin', array_merge([
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'domain' => $domain,
					'page' => $page,
				],
				$post,
				[
					'sort' => $sort,
				])
		);
	}
	public static function postView($domain, $id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'post/view', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
			'id' => $id,
		]);
	}
	public static function postCreate($domain, array $Post) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($Post as $k => $v) {
			$post['Post[' . $k . ']'] = $v;
		}

		return self::execute(self::$service,
			'post/create', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
			],
			$post
		);
	}
	public static function postDelete($domain, $id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'post/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
			'id' => $id,
		]);
	}
	public static function postUpdate($domain, $id, array $Post = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		if($Post === []) {
			return self::execute(self::$service, 'post/update', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
				'id' => $id,
			]);
		} else {
			$post = [];
			foreach ($Post as $k => $v) {
				$post['Post[' . $k . ']'] = $v;
			}

			return self::execute(self::$service,
				'post/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'domain' => $domain,
					'id' => $id,
				],
				$post
			);
		}
	}
	public static function tagList($domain) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'tag/list', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
		]);
	}
	public static function tagDelete($domain, $tag) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'tag/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
			'tag' => $tag,
		]);
	}
	public static function tagUpdate($domain, $tag, array $TagForm) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($TagForm as $k => $v) {
			$post['TagForm[' . $k . ']'] = $v;
		}

		return self::execute(self::$service, 'tag/update', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
			'tag' => $tag,
		], $post);
	}
	public static function siteList() {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'site/list', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
		]);
	}
	public static function siteCreate(array $Site) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($Site as $k => $v) {
			$post['Site[' . $k . ']'] = $v;
		}

		return self::execute(self::$service, 'site/create', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
		], $post);
	}
	public static function siteDelete($domain) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'site/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
		]);
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public static function siteIsSet($id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		$res = self::execute(self::$service, 'site/is-set', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'id' => $id,
		]);
		return $res['result'] == 'error' ? false : true;
	}
	public static function siteUpdate($domain, array $Site = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		if($Site === []) {
			return self::execute(self::$service, 'site/update', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
			]);
		} else {
			$post = [];
			foreach ($Site as $k => $v) {
				$post['Site[' . $k . ']'] = $v;
			}

			return self::execute(self::$service, 'site/update', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
			], $post);
		}
	}
}