<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext\api\auth;


class AuthUrlCreator {
	public static function userGetMark($service, $callback) {
		$rand = \Yii::$app->security->generateRandomString(mt_rand(15, 20));
		return '//'
		. \Yii::$app->params['api']['urls']['auth']
		. 'user/get-mark'
		. '?service=' . $service
		. '&callback=' . $callback
		. '&rand=' . urlencode($rand)
		. '&sign=' . md5(
			$service
			. md5('service=' . $service
				. '&callback=' . $callback
				. '&rand=' . $rand)
			. 'get-mark'
			. \Yii::$app->params['api']['secrets'][ $service ]);
	}
	public static function userIsAuthenticate($service, $callback) {
		$rand = \Yii::$app->security->generateRandomString(mt_rand(15, 20));
		return '//'
		. \Yii::$app->params['api']['urls']['auth']
		. 'user/is-authenticate'
		. '?service=' . $service
		. '&callback=' . $callback
		. '&rand=' . urlencode($rand)
		. '&sign=' . md5(
			$service
			. md5('service=' . $service
				. '&callback=' . $callback
				. '&rand=' . $rand)
			. 'is-authenticate'
			. \Yii::$app->params['api']['secrets'][ $service ]);
	}
	public static function userAuthenticate($service, $view, $retUrl) {
		$rand = \Yii::$app->security->generateRandomString(mt_rand(15, 20));
		return '//'
		. \Yii::$app->params['api']['urls']['auth']
		. 'user/authenticate'
		. '?service=' . $service
		. '&view=' . $view
		. '&retUrl=' . urlencode($retUrl)
		. '&rand=' . urlencode($rand)
		. '&sign=' . md5(
			$service
			. md5('service=' . $service
				. '&view=' . $view
				. '&retUrl=' . $retUrl
				. '&rand=' . $rand)
			. 'authenticate'
			. \Yii::$app->params['api']['secrets'][ $service ]);
	}
	public static function userLogout($service, $retUrl) {
		$rand = \Yii::$app->security->generateRandomString(mt_rand(15, 20));
		return '//'
		. \Yii::$app->params['api']['urls']['auth']
		. 'user/logout'
		. '?service=' . $service
		. '&retUrl=' . urlencode($retUrl)
		. '&rand=' . urlencode($rand)
		. '&sign=' . md5(
			$service
			. md5('service=' . $service
				. '&retUrl=' . $retUrl
				. '&rand=' . $rand)
			. 'logout'
			. \Yii::$app->params['api']['secrets'][ $service ]);
	}
	public static function userLogoutEverywhere($service, $retUrl) {
		$rand = \Yii::$app->security->generateRandomString(mt_rand(15, 20));
		return '//'
		. \Yii::$app->params['api']['urls']['auth']
		. 'user/logout-everywhere'
		. '?service=' . $service
		. '&retUrl=' . urlencode($retUrl)
		. '&rand=' . urlencode($rand)
		. '&sign=' . md5(
			$service
			. md5('service=' . $service
				. '&retUrl=' . $retUrl
				. '&rand=' . $rand)
			. 'logout-everywhere'
			. \Yii::$app->params['api']['secrets'][ $service ]);
	}
	public static function userSignup($service, $view, $retUrl) {
		$rand = \Yii::$app->security->generateRandomString(mt_rand(15, 20));
		return '//'
		. \Yii::$app->params['api']['urls']['auth']
		. 'user/signup-form'
		. '?service=' . $service
		. '&view=' . $view
		. '&retUrl=' . urlencode($retUrl)
		. '&rand=' . urlencode($rand)
		. '&sign=' . md5(
			$service
			. md5('service=' . $service
				. '&view=' . $view
				. '&retUrl=' . $retUrl
				. '&rand=' . $rand)
			. 'signup-form'
			. \Yii::$app->params['api']['secrets'][ $service ]);
	}
} 