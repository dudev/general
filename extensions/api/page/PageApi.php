<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext\api\page;


use general\ext\api\BaseApi;

class PageApi extends BaseApi {
	private static $service = 'page';
	public static function pageList($domain) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'page/list', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
		]);
	}
	public static function pageView($domain, $id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'page/view', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
			'id' => $id,
		]);
	}
	public static function pageViewByNick($domain, $nick) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'page/view-by-nick', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
			'nick' => $nick,
		]);
	}
	public static function pageCreate($domain, array $Page) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($Page as $k => $v) {
			$post['Page[' . $k . ']'] = $v;
		}

		return self::execute(self::$service,
			'page/create', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
			],
			$post
		);
	}
	public static function pageDelete($domain, $id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'post/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
			'id' => $id,
		]);
	}
	public static function pageUpdate($domain, $id, array $Page = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		if($Page === []) {
			return self::execute(self::$service, 'page/update', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
				'id' => $id,
			]);
		} else {
			$post = [];
			foreach ($Page as $k => $v) {
				$post['Page[' . $k . ']'] = $v;
			}

			return self::execute(self::$service,
				'page/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'domain' => $domain,
					'id' => $id,
				],
				$post
			);
		}
	}
	public static function templateList($domain) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'template/list', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
		]);
	}
	public static function templateDelete($domain, $id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'template/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
			'id' => $id,
		]);
	}
	public static function templateCreate($domain, array $Template) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($Template as $k => $v) {
			$post['Template[' . $k . ']'] = $v;
		}

		return self::execute(self::$service, 'template/create', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
		], $post);
	}
	public static function templateUpdate($domain, $id, array $Template = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		if($Template === []) {
			return self::execute(self::$service, 'template/update', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
				'id' => $id,
			]);
		} else {
			$post = [];
			foreach ($Template as $k => $v) {
				$post['Template[' . $k . ']'] = $v;
			}

			return self::execute(self::$service, 'template/update', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
				'id' => $id,
			], $post);
		}
	}
	public static function siteList() {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'site/list', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
		]);
	}
	public static function siteCreate(array $Site) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($Site as $k => $v) {
			$post['Site[' . $k . ']'] = $v;
		}

		return self::execute(self::$service, 'site/create', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
		], $post);
	}
	public static function siteDelete($domain) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'site/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'domain' => $domain,
		]);
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public static function siteIsSet($id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		$res = self::execute(self::$service, 'site/is-set', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'id' => $id,
		]);
		return $res['result'] == 'error' ? false : true;
	}
	public static function siteView($id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'site/view', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'id' => $id,
		]);
	}
	public static function siteUpdate($domain, array $Site = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		if($Site === []) {
			return self::execute(self::$service, 'site/update', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
			]);
		} else {
			$post = [];
			foreach ($Site as $k => $v) {
				$post['Site[' . $k . ']'] = $v;
			}

			return self::execute(self::$service, 'site/update', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'domain' => $domain,
			], $post);
		}
	}
}