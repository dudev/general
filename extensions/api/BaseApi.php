<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext\api;


use app\models\ReceivedAccessKeys;
use general\controllers\api\Controller;
use yii\base\Object;
use yii\db\Query;
use yii\log\Logger;

class BaseApi extends Object {
	//выданные ключи
	private static $iss_ak = [];
	//полученные ключи
	private static $rec_ak = [];

	/**
	 * Проверка на наличие созданного сайта в сервисе
	 * @param $id - site_id
	 * @return bool
	 */
	public static function siteIsSet($id) {
		return true;
	}

	public static function getSites() {
		$ret = [];
		$data = static::siteList();
		if($data['result'] == 'success') {
			foreach ($data['sites'] as $site) {
				$ret[ $site['id'] ] = $site['name'];
			}

		}
		return $ret;
	}

	public static function getInplementationName($service) {
		return '\\general\\ext\api\\' . mb_strtolower($service) . '\\' . ucfirst($service) . 'Api';
	}

	/**
	 * Получить access_key выданный сервисом $service текущему сервису
	 * @param string $service Ник сервиса
	 * @return string|false(при ошибке)
	 */
	protected static function getReceivedAccessKey($service) {
		if((!isset(self::$rec_ak[ $service ]) //если ключ еще не получен из БД
			|| self::$rec_ak[ $service ]->expires_in < time()) //или он истёк
		&& (!(self::$rec_ak[ $service ] = ReceivedAccessKeys::findOne(['service' => $service])) //если ключа нет в БД
			|| self::$rec_ak[ $service ]->expires_in < time()) //или он истёк
		) {
			if(!self::$rec_ak[ $service ]) {
				self::$rec_ak[ $service ] = new ReceivedAccessKeys();
			}
			if(!$key = self::authGetAccessKey($service)) {
				unset(self::$rec_ak[ $service ]);
				return false;
			}
			self::$rec_ak[ $service ]->access_key = $key['access_key'];
			self::$rec_ak[ $service ]->expires_in = $key['expires_in'];
			self::$rec_ak[ $service ]->service = $service;
			if(!self::$rec_ak[ $service ]->save()) {
				unset(self::$rec_ak[ $service ]);
				return false;
			}
		}
		return self::$rec_ak[ $service ]->access_key;
	}
	/**
	 * Получить access_key у сервиса $service текущему сервису
	 * @param string $service Ник сервиса
	 * @return array|false(при ошибке)
	 */
	private static function authGetAccessKey($service) {
		$ans = self::execute($service, 'auth/get-access-key', [
			'service' => \Yii::$app->id,
			'secret_key' => \Yii::$app->params['api']['secrets'][ \Yii::$app->id ],
		]);
		return $ans['result'] == 'error' ? false : $ans;
	}
	/**
	 * Выполняет запрос к API сервиса $service к методу $method
	 * @param string $service Ник сервиса
	 * @param string $method Метода
	 * @param array $get Параметры GET
	 * @param array $post Параметры POST
	 * @param bool $secure использовать защищённое соединение
	 * @return array
	 */
	protected static function execute($service, $method, $get, $post = [], $secure = true) { //@todo $secure = true
		$url = ($secure ? 'https' : 'http')
			. '://'
			. \Yii::$app->params['api']['urls'][ $service ]
			. $method
			. '?';
		$url .= http_build_query($get, '', '&', PHP_QUERY_RFC3986);

		\Yii::getLogger()->log('Внешний запрос: ' . $url, Logger::LEVEL_INFO);
		if($post) {
			\Yii::getLogger()->log('POST: ' . var_export($post, true), Logger::LEVEL_INFO);
		}
		\Yii::getLogger()->log('Внешний запрос: ' . $url, Logger::LEVEL_PROFILE_BEGIN);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		if($post) {
			curl_setopt_array($ch,
				[
					CURLOPT_POST => true,
					CURLOPT_SAFE_UPLOAD => true,
					CURLOPT_HTTPHEADER => array('Content-Type: multipart/form-data'),
					CURLOPT_POSTFIELDS => $post,
				]
			);
		}
		$ret = curl_exec($ch);
		curl_close($ch);

		\Yii::getLogger()->log('Внешний запрос: ' . $url, Logger::LEVEL_PROFILE_END);
		\Yii::getLogger()->log('Ответ на внешний запрос: ' . $ret, Logger::LEVEL_INFO);

		$ret = json_decode($ret, true);
		return (!$ret || !isset($ret['result'])) ? [
			'result' => 'error',
			'errors' => [
				[
					'code' => Controller::ERROR_UNKNOWN,
					'title' => Controller::$_errors[ Controller::ERROR_UNKNOWN ],
				],
			],
		] : $ret;
	}
}