<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
return [
	'contentServer' => '',
	'dateFormat' => 'd.m.Y H:h',
];