<?php
/**
 * Project: Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\controllers\api;


use app\models\IssuedAccessKeys;
use yii\db\Query;
use yii\web\Response;

class Controller extends \yii\web\Controller {
	const ERROR_UNKNOWN = 1;
	const ERROR_DB = 2;
	const ERROR_TOO_BIG_PAGE_NUMBER = 3;
	const ERROR_HTTP_FORBIDDEN = 4;
	const ERROR_ILLEGAL_REQUEST_METHOD = 5;
	const ERROR_NO_DATA = 6;
	const ERROR_IN_FOREIGN_QUERY = 7;

	const ERROR_FORBIDDEN = 101;
	const ERROR_ILLEGAL_SECRET_KEY = 102;
	const ERROR_ILLEGAL_ACCESS_KEY = 103;

	const ERROR_NO_USER = 201;
	const ERROR_ILLEGAL_LOGIN = 202;
	const ERROR_ILLEGAL_PASSWORD = 203;
	const ERROR_ILLEGAL_STATUS = 204;
	const ERROR_NOT_AUTHENTICATED = 205;
	const ERROR_INCORRECT_LOGIN_PASSWORD = 206;
	const ERROR_INCORRECT_MARK = 207;
	const ERROR_ILLEGAL_QUESTION = 208;
	const ERROR_ILLEGAL_ANSWER = 209;
	const ERROR_ILLEGAL_CALLBACK = 210;
	const ERROR_INCORRECT_SIGN = 211;
	const ERROR_ILLEGAL_SCHEDULE = 212;

	const ERROR_INCORRECT_ANSWER = 301;
	const ERROR_NO_SECRET_QUESTION = 302;

	const ERROR_INCORRECT_HASH = 401;

	const ERROR_NO_POST = 501;
	const ERROR_ILLEGAL_NICK = 502;
	const ERROR_ILLEGAL_TITLE = 503;
	const ERROR_ILLEGAL_KEYWORDS = 504;
	const ERROR_ILLEGAL_DESCRIPTION = 505;
	const ERROR_ILLEGAL_TEXT = 506;
	const ERROR_ILLEGAL_TAGS = 507;
	const ERROR_ILLEGAL_POST_STATUS = 508;
	const ERROR_ILLEGAL_PUBLISH_DATE = 509;
	const ERROR_ILLEGAL_FIELD_PICTURE = 510;
	const ERROR_ILLEGAL_FIELD_PICTURE_IN_POST = 511;
	const ERROR_ILLEGAL_PICTURE = 512;

	const ERROR_NO_TAGS = 601;

	const ERROR_ILLEGAL_SITE_NAME = 701;
	const ERROR_ILLEGAL_SITE_DOMAIN = 702;
	const ERROR_NO_SITE = 703;

	const ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NAME = 801;
	const ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_PARENT_ID = 802;
	const ERROR_NO_CATEGORY_OF_ARTICLE = 803;
	const ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NUMBER = 804;

	const ERROR_NO_PRODUCT = 901;
	const ERROR_ILLEGAL_PRODUCT_NAME = 902;
	const ERROR_ILLEGAL_PRODUCT_DESCRIPTION = 903;
	const ERROR_ILLEGAL_PRODUCT_PROTEIN = 904;
	const ERROR_ILLEGAL_PRODUCT_FAT = 905;
	const ERROR_ILLEGAL_PRODUCT_CARBOHYDRATE = 906;
	const ERROR_ILLEGAL_PRODUCT_CALORIE = 907;
	const ERROR_ILLEGAL_PRODUCT_DIETARY_FIBER = 908;
	const ERROR_ILLEGAL_PRODUCT_CHOLESTEROL = 909;
	const ERROR_ILLEGAL_PRODUCT_WATER = 910;
	const ERROR_ILLEGAL_PRODUCT_CA = 911;
	const ERROR_ILLEGAL_PRODUCT_FE = 912;
	const ERROR_ILLEGAL_PRODUCT_NA = 913;
	const ERROR_ILLEGAL_PRODUCT_K = 914;
	const ERROR_ILLEGAL_PRODUCT_MG = 915;
	const ERROR_ILLEGAL_PRODUCT_P = 916;
	const ERROR_ILLEGAL_PRODUCT_CO = 917;
	const ERROR_ILLEGAL_PRODUCT_CR = 918;
	const ERROR_ILLEGAL_PRODUCT_CU = 919;
	const ERROR_ILLEGAL_PRODUCT_MN = 920;
	const ERROR_ILLEGAL_PRODUCT_S = 921;
	const ERROR_ILLEGAL_PRODUCT_SI = 922;
	const ERROR_ILLEGAL_PRODUCT_V = 923;
	const ERROR_ILLEGAL_PRODUCT_ZN = 924;
	const ERROR_ILLEGAL_PRODUCT_MO = 925;
	const ERROR_ILLEGAL_PRODUCT_F = 926;
	const ERROR_ILLEGAL_PRODUCT_I = 927;
	const ERROR_ILLEGAL_PRODUCT_SE = 928;
	const ERROR_ILLEGAL_PRODUCT_B = 929;
	const ERROR_ILLEGAL_PRODUCT_CL = 930;
	const ERROR_ILLEGAL_PRODUCT_A = 931;
	const ERROR_ILLEGAL_PRODUCT_B_CAR = 932;
	const ERROR_ILLEGAL_PRODUCT_B1 = 933;
	const ERROR_ILLEGAL_PRODUCT_B2 = 934;
	const ERROR_ILLEGAL_PRODUCT_B4 = 935;
	const ERROR_ILLEGAL_PRODUCT_B5 = 936;
	const ERROR_ILLEGAL_PRODUCT_B6 = 937;
	const ERROR_ILLEGAL_PRODUCT_B7 = 938;
	const ERROR_ILLEGAL_PRODUCT_B9 = 939;
	const ERROR_ILLEGAL_PRODUCT_B12 = 940;
	const ERROR_ILLEGAL_PRODUCT_C = 941;
	const ERROR_ILLEGAL_PRODUCT_D = 942;
	const ERROR_ILLEGAL_PRODUCT_E = 943;
	const ERROR_ILLEGAL_PRODUCT_PP = 944;
	const ERROR_ILLEGAL_PRODUCT_K_PHYLLOQUINONE = 945;
	const ERROR_ILLEGAL_PRODUCT_TRYPTOPHAN = 946;
	const ERROR_ILLEGAL_PRODUCT_LYSINE = 947;
	const ERROR_ILLEGAL_PRODUCT_METHIONINE = 948;
	const ERROR_ILLEGAL_PRODUCT_THREONINE = 949;
	const ERROR_ILLEGAL_PRODUCT_LEUCINE = 950;
	const ERROR_ILLEGAL_PRODUCT_ISOLEUCINE = 951;
	const ERROR_ILLEGAL_PRODUCT_PHENYLALANINE = 952;
	const ERROR_ILLEGAL_PRODUCT_VALINE = 953;
	const ERROR_ILLEGAL_PRODUCT_OWNER_ID = 954;
	const ERROR_ILLEGAL_PRODUCT_COPY_FROM = 955;
	const ERROR_ILLEGAL_PRODUCT_CATEGORY_ID = 956;
	const ERROR_ILLEGAL_PRODUCT_IMAGE = 957;

	const ERROR_NO_PASSPORT = 1001;
	const ERROR_ILLEGAL_PASSPORT_FIRSTNAME = 1002;
	const ERROR_ILLEGAL_PASSPORT_LASTNAME = 1003;
	const ERROR_ILLEGAL_PASSPORT_FATHERNAME = 1004;
	const ERROR_ILLEGAL_PASSPORT_PHONE = 1005;
	const ERROR_ILLEGAL_PASSPORT_EMAIL = 1006;
	const ERROR_ILLEGAL_PASSPORT_USER_ID = 1007;
	const ERROR_ILLEGAL_PASSPORT_INFO = 1008;
	const ERROR_ILLEGAL_PASSPORT_SEX = 1009;
	const ERROR_ILLEGAL_PASSPORT_BIRTHDAY = 1010;

	const ERROR_NO_SITE_COMMENT = 1101;
	const ERROR_ILLEGAL_SITE_NAME_COMMENT = 1102;
	const ERROR_ILLEGAL_SITE_ANONYM_COMMENT = 1103;
	const ERROR_ILLEGAL_SITE_GET_PARAMS = 1104;
	const ERROR_ILLEGAL_PRE_MODERATION = 1105;
	const ERROR_ILLEGAL_TREE = 1106;
	const ERROR_ILLEGAL_OWNER = 1107;
	const ERROR_ILLEGAL_SITE_DOMAIN_COMMENT = 1108;

	const ERROR_ILLEGAL_COMMENT = 1201;
	const ERROR_ILLEGAL_COMMENT_PAGE_PATH = 1202;
	const ERROR_ILLEGAL_COMMENT_ANSWER_TO = 1203;
	const ERROR_ILLEGAL_COMMENT_IS_PUBLIC = 1204;
	const ERROR_NO_COMMENT = 1205;

	const ERROR_NO_PAGE = 1301;
	const ERROR_ILLEGAL_PAGE_NICK = 1302;
	const ERROR_ILLEGAL_PAGE_TITLE = 1303;
	const ERROR_ILLEGAL_PAGE_KEYWORDS = 1304;
	const ERROR_ILLEGAL_PAGE_DESCRIPTION = 1305;
	const ERROR_ILLEGAL_PAGE_TEXT = 1306;
	const ERROR_ILLEGAL_PAGE_TEMPLATE = 1307;

	const ERROR_NO_TEMPLATE = 1401;
	const ERROR_ILLEGAL_TEMPLATE_NAME = 1402;
	const ERROR_ILLEGAL_TEMPLATE_HEAD = 1403;
	const ERROR_ILLEGAL_TEMPLATE_BODY_START = 1404;
	const ERROR_ILLEGAL_TEMPLATE_BODY_END = 1405;

	public static $_errors = [
		self::ERROR_UNKNOWN => 'Неизвестная ошибка.',
		self::ERROR_DB => 'Ошибка при работе с базой данных.',
		self::ERROR_TOO_BIG_PAGE_NUMBER => 'Слишком большой номер страницы.',
		self::ERROR_HTTP_FORBIDDEN => 'Обращения по незащищенному соединению запрещены.',
		self::ERROR_ILLEGAL_REQUEST_METHOD => 'Недопустимый метод.',
		self::ERROR_NO_DATA => 'Данные не получены.',
		self::ERROR_IN_FOREIGN_QUERY => 'Ошибка во внешнем запросе.',

		self::ERROR_FORBIDDEN => 'Запрет операции для сервиса.',
		self::ERROR_ILLEGAL_SECRET_KEY => 'Неверный ник сервиса или постоянный ключ.',
		self::ERROR_ILLEGAL_ACCESS_KEY => 'Неверный ключ авторизации или ник сервиса.',

		self::ERROR_NO_USER => 'Нет совпадений при поиске пользователей.',
		self::ERROR_ILLEGAL_LOGIN => 'Недопустимый логин.',
		self::ERROR_ILLEGAL_PASSWORD => 'Недопустимый пароль.',
		self::ERROR_ILLEGAL_STATUS => 'Недопустимый статус.',
		self::ERROR_NOT_AUTHENTICATED => 'Пользователь не аутентифицирован.',
		self::ERROR_INCORRECT_LOGIN_PASSWORD => 'Неверный логин или пароль.',
		self::ERROR_INCORRECT_MARK => 'Неверная временная метка.',
		self::ERROR_ILLEGAL_QUESTION => 'Недопустимый секретный вопрос.',
		self::ERROR_ILLEGAL_ANSWER => 'Недопустимый ответ на секретный вопрос.',
		self::ERROR_ILLEGAL_CALLBACK => 'Недопустимое имя callback-функции.',
		self::ERROR_INCORRECT_SIGN => 'Неверная подпись',
		self::ERROR_ILLEGAL_SCHEDULE => 'Недопустимое имя шаблона.',

		self::ERROR_INCORRECT_ANSWER => 'Неверный ответ на секретный вопрос.',
		self::ERROR_NO_SECRET_QUESTION => 'Секретный вопрос не задан.',

		self::ERROR_INCORRECT_HASH => 'Неверный хэш-код.',

		self::ERROR_NO_POST => 'Записей не найдено.',
		self::ERROR_ILLEGAL_NICK => 'Недопустимый ник.',
		self::ERROR_ILLEGAL_TITLE => 'Недопустимый заголовок.',
		self::ERROR_ILLEGAL_KEYWORDS => 'Недопустимые ключевые слова.',
		self::ERROR_ILLEGAL_DESCRIPTION => 'Недопустимое описание.',
		self::ERROR_ILLEGAL_TEXT => 'Недопустимый текст.',
		self::ERROR_ILLEGAL_TAGS => 'Недопустимые метки.',
		self::ERROR_ILLEGAL_POST_STATUS => 'Недопустимый статус.',
		self::ERROR_ILLEGAL_PUBLISH_DATE => 'Недопустимая дата публикации.',
		self::ERROR_ILLEGAL_FIELD_PICTURE => 'Недопустимое значение поля Наличие картинки.',
		self::ERROR_ILLEGAL_FIELD_PICTURE_IN_POST => 'Недопустимое значение поля Показывать картинку в записи.',
		self::ERROR_ILLEGAL_PICTURE => 'Ошибка загрузки картинки.',

		self::ERROR_NO_TAGS => 'Меток не найдено.',

		self::ERROR_ILLEGAL_SITE_NAME => 'Недопустимое название блога.',
		self::ERROR_ILLEGAL_SITE_DOMAIN => 'Недопустимый домен (ник блога).',
		self::ERROR_NO_SITE => 'Блог не найден.',

		self::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NAME => 'Недопустимое название.',
		self::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_PARENT_ID => 'Недопустимый ID родителя.',
		self::ERROR_NO_CATEGORY_OF_ARTICLE => 'Категория не найдена.',
		self::ERROR_ILLEGAL_CATEGORY_OF_ARTICLE_NUMBER => 'Недопустимый порядковый номер.',

		self::ERROR_NO_PRODUCT => 'Не найдено ни одного продукта.',
		self::ERROR_ILLEGAL_PRODUCT_NAME => 'Неверно заполнено поле «Название».',
		self::ERROR_ILLEGAL_PRODUCT_DESCRIPTION => 'Неверно заполнено поле «Описание».',
		self::ERROR_ILLEGAL_PRODUCT_PROTEIN => 'Неверно заполнено поле «Белки».',
		self::ERROR_ILLEGAL_PRODUCT_FAT => 'Неверно заполнено поле «Жиры».',
		self::ERROR_ILLEGAL_PRODUCT_CARBOHYDRATE => 'Неверно заполнено поле «Углеводы».',
		self::ERROR_ILLEGAL_PRODUCT_CALORIE => 'Неверно заполнено поле «Калорийность».',
		self::ERROR_ILLEGAL_PRODUCT_DIETARY_FIBER => 'Неверно заполнено поле «Пищевые волокна».',
		self::ERROR_ILLEGAL_PRODUCT_CHOLESTEROL => 'Неверно заполнено поле «Холестерин».',
		self::ERROR_ILLEGAL_PRODUCT_WATER => 'Неверно заполнено поле «Процент воды».',
		self::ERROR_ILLEGAL_PRODUCT_CA => 'Неверно заполнено поле «Кальций, мг».',
		self::ERROR_ILLEGAL_PRODUCT_FE => 'Неверно заполнено поле «Железо, мг».',
		self::ERROR_ILLEGAL_PRODUCT_NA => 'Неверно заполнено поле «Натрий, мг».',
		self::ERROR_ILLEGAL_PRODUCT_K => 'Неверно заполнено поле «Калий, мг».',
		self::ERROR_ILLEGAL_PRODUCT_MG => 'Неверно заполнено поле «Магний, мг».',
		self::ERROR_ILLEGAL_PRODUCT_P => 'Неверно заполнено поле «P, мг».',
		self::ERROR_ILLEGAL_PRODUCT_CO => 'Неверно заполнено поле «Co, мг».',
		self::ERROR_ILLEGAL_PRODUCT_CR => 'Неверно заполнено поле «Cr, мг».',
		self::ERROR_ILLEGAL_PRODUCT_CU => 'Неверно заполнено поле «Cu, мг».',
		self::ERROR_ILLEGAL_PRODUCT_MN => 'Неверно заполнено поле «Mn, мг».',
		self::ERROR_ILLEGAL_PRODUCT_S => 'Неверно заполнено поле «S, мг».',
		self::ERROR_ILLEGAL_PRODUCT_SI => 'Неверно заполнено поле «Si, мг».',
		self::ERROR_ILLEGAL_PRODUCT_V => 'Неверно заполнено поле «V, мг».',
		self::ERROR_ILLEGAL_PRODUCT_ZN => 'Неверно заполнено поле «Zn, мг».',
		self::ERROR_ILLEGAL_PRODUCT_MO => 'Неверно заполнено поле «Mo, мг».',
		self::ERROR_ILLEGAL_PRODUCT_F => 'Неверно заполнено поле «F, мг».',
		self::ERROR_ILLEGAL_PRODUCT_I => 'Неверно заполнено поле «I, мг».',
		self::ERROR_ILLEGAL_PRODUCT_SE => 'Неверно заполнено поле «Se, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B => 'Неверно заполнено поле «B, мг».',
		self::ERROR_ILLEGAL_PRODUCT_CL => 'Неверно заполнено поле «Cl, мг».',
		self::ERROR_ILLEGAL_PRODUCT_A => 'Неверно заполнено поле «A, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B_CAR => 'Неверно заполнено поле «B-car, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B1 => 'Неверно заполнено поле «B1, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B2 => 'Неверно заполнено поле «B2, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B4 => 'Неверно заполнено поле «B4, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B5 => 'Неверно заполнено поле «B5, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B6 => 'Неверно заполнено поле «B6, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B7 => 'Неверно заполнено поле «B7, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B9 => 'Неверно заполнено поле «B9, мг».',
		self::ERROR_ILLEGAL_PRODUCT_B12 => 'Неверно заполнено поле «B12, мг».',
		self::ERROR_ILLEGAL_PRODUCT_C => 'Неверно заполнено поле «C, мг».',
		self::ERROR_ILLEGAL_PRODUCT_D => 'Неверно заполнено поле «D, мг».',
		self::ERROR_ILLEGAL_PRODUCT_E => 'Неверно заполнено поле «E, мг».',
		self::ERROR_ILLEGAL_PRODUCT_PP => 'Неверно заполнено поле «PP, мг».',
		self::ERROR_ILLEGAL_PRODUCT_K_PHYLLOQUINONE => 'Неверно заполнено поле «K (филлохинон), мг».',
		self::ERROR_ILLEGAL_PRODUCT_TRYPTOPHAN => 'Неверно заполнено поле «Триптофан, мг».',
		self::ERROR_ILLEGAL_PRODUCT_LYSINE => 'Неверно заполнено поле «Лизин, мг».',
		self::ERROR_ILLEGAL_PRODUCT_METHIONINE => 'Неверно заполнено поле «Метионин, мг».',
		self::ERROR_ILLEGAL_PRODUCT_THREONINE => 'Неверно заполнено поле «Треонин, мг».',
		self::ERROR_ILLEGAL_PRODUCT_LEUCINE => 'Неверно заполнено поле «Лейцин, мг».',
		self::ERROR_ILLEGAL_PRODUCT_ISOLEUCINE => 'Неверно заполнено поле «Изолейцин, мг».',
		self::ERROR_ILLEGAL_PRODUCT_PHENYLALANINE => 'Неверно заполнено поле «Фенилаланин, мг».',
		self::ERROR_ILLEGAL_PRODUCT_VALINE => 'Неверно заполнено поле «Валин, мг».',
		self::ERROR_ILLEGAL_PRODUCT_OWNER_ID => 'Неверно заполнено поле «Владелец».',
		self::ERROR_ILLEGAL_PRODUCT_COPY_FROM => 'Неверно заполнено поле «Родитель».',
		self::ERROR_ILLEGAL_PRODUCT_CATEGORY_ID => 'Неверно заполнено поле «Категория».',
		self::ERROR_ILLEGAL_PRODUCT_IMAGE => 'Неверно заполнено поле «Картинка».',

		self::ERROR_NO_PASSPORT => 'Нет совпадений при поиске.',
		self::ERROR_ILLEGAL_PASSPORT_FIRSTNAME => 'Недопустимое имя.',
		self::ERROR_ILLEGAL_PASSPORT_LASTNAME => 'Недопустимая фамилия.',
		self::ERROR_ILLEGAL_PASSPORT_FATHERNAME => 'Недопустимое отчество.',
		self::ERROR_ILLEGAL_PASSPORT_PHONE => 'Недопустимый телефон.',
		self::ERROR_ILLEGAL_PASSPORT_EMAIL => 'Недопустимая эл. почта.',
		self::ERROR_ILLEGAL_PASSPORT_USER_ID => 'Недопустимый ID пользователя.',
		self::ERROR_ILLEGAL_PASSPORT_INFO => 'Недопустимая дополнительная информация.',
		self::ERROR_ILLEGAL_PASSPORT_SEX => 'Недопустимый пол.',
		self::ERROR_ILLEGAL_PASSPORT_BIRTHDAY => 'Недопустимая дата рождения.',

		self::ERROR_NO_SITE_COMMENT => 'Не найдено.',
		self::ERROR_ILLEGAL_SITE_NAME_COMMENT => 'Недопустимое название.',
		self::ERROR_ILLEGAL_SITE_ANONYM_COMMENT => 'Недопустимое значение в поле «Разрешить анонимные комментарии».',
		self::ERROR_ILLEGAL_SITE_GET_PARAMS => 'Недопустимое значение в поле «Учитываемые GET-параметры».',
		self::ERROR_ILLEGAL_PRE_MODERATION => 'Недопустимое значение в поле «Премодерация».',
		self::ERROR_ILLEGAL_TREE => 'Недопустимое значение в поле «Древовидный вид».',
		self::ERROR_ILLEGAL_OWNER => 'Недопустимый владелец.',
		self::ERROR_ILLEGAL_SITE_DOMAIN_COMMENT => 'Недопустимый домен.',

		self::ERROR_ILLEGAL_COMMENT => 'Недопустимый комментарий.',
		self::ERROR_ILLEGAL_COMMENT_PAGE_PATH => 'Недопустимый путь до страницы.',
		self::ERROR_ILLEGAL_COMMENT_ANSWER_TO => 'Недопустимый комментарий-родитель.',
		self::ERROR_ILLEGAL_COMMENT_IS_PUBLIC => 'Недопустимое значение в поле «Опубликовать».',
		self::ERROR_NO_COMMENT => 'Не найдено.',

		self::ERROR_NO_PAGE => 'Записей не найдено.',
		self::ERROR_ILLEGAL_PAGE_NICK => 'Недопустимый ник.',
		self::ERROR_ILLEGAL_PAGE_TITLE => 'Недопустимый заголовок.',
		self::ERROR_ILLEGAL_PAGE_KEYWORDS => 'Недопустимые ключевые слова.',
		self::ERROR_ILLEGAL_PAGE_DESCRIPTION => 'Недопустимое описание.',
		self::ERROR_ILLEGAL_PAGE_TEXT => 'Недопустимый текст.',
		self::ERROR_ILLEGAL_PAGE_TEMPLATE => 'Недопустимый шаблон.',

		self::ERROR_NO_TEMPLATE => 'Шаблонов не найдено.',
		self::ERROR_ILLEGAL_TEMPLATE_NAME => 'Недопустимое название.',
		self::ERROR_ILLEGAL_TEMPLATE_HEAD => 'Недопустимое значение в поле head.',
		self::ERROR_ILLEGAL_TEMPLATE_BODY_START => 'Недопустимое значение в поле body_start.',
		self::ERROR_ILLEGAL_TEMPLATE_BODY_END => 'Недопустимое значение в поле body_end.',
	];

	protected $_safe_actions = [];
	/** @var IssuedAccessKeys $_access_key */
	protected $_access_key = null;

	public function beforeAction($action) {
		$this->enableCsrfValidation = false;

		if(!in_array($action->id, $this->_safe_actions, true)) {
			$this->enableCsrfValidation = false;
		}
		if (parent::beforeAction($action)) {
			if(in_array($action->id, $this->_safe_actions, true)) {
				return true;
			}
			return $this->checkAccessKey($action) && $this->checkConnection($action);
		} else {
			return false;
		}
	}
	private function checkConnection($action) {
		//@todo включить запрет соединения по незащищенному соединению
		/*if(!YII_ENV_DEV && !\Yii::$app->getRequest()->getIsSecureConnection()) {
			echo json_encode($this->sendError(self::ERROR_HTTP_FORBIDDEN));
			return false;
		}*/
		return true;
	}
	private function checkAccessKey($action) {
		if(in_array($action->id, $this->_safe_actions, true)) {
			return true;
		}

		$request = \Yii::$app->request;

		$this->_access_key = IssuedAccessKeys::findOne([
			'access_key' => $request->get('access_key'),
			'service' => $request->get('service'),
		]);

		if($this->_access_key && $this->_access_key->expires_in > time()) {
			return true;
		}
		echo json_encode($this->sendError(self::ERROR_ILLEGAL_ACCESS_KEY));
		return false;
	}
	protected function sendSuccess($data) {
		\Yii::$app->response->format = Response::FORMAT_JSON;
		return array_merge([
				'result' => 'success'
			],
			$data
		);
	}
	protected function sendError($codes) {
		\Yii::$app->response->format = Response::FORMAT_JSON;
		if(!$codes) {
			$codes = self::ERROR_UNKNOWN;
		}
		foreach ((array)$codes as $code) {
			$errors[] = [
				'code' => $code,
				'title' => isset(self::$_errors[ $code ]) ? self::$_errors[ $code ] : self::$_errors[ self::ERROR_UNKNOWN ],
			];
		}
		return [
			'result' => 'error',
			'errors' => $errors,
		];
	}
	protected function getErrorCodes($error_codes, $model) {
		foreach ($model->getFirstErrors() as $attr => $err) {
			if (isset($error_codes[$attr])) {
				$errors[] = $error_codes[$attr];
			}
		}
		return isset($errors) ? $errors : null;
	}
} 