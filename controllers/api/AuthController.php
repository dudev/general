<?php
/**
 * Project: Auth - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\controllers\api;

use app\models\IssuedAccessKeys;
use general\ext\api\ApiController;

class AuthController extends Controller {
	protected $_safe_actions = [
		'get-access-key',
	];

	/**
	 * @param $secret_key
	 * @param $service
	 * @return array
	 */
	public function actionGetAccessKey($secret_key, $service) {
		if($service != \Yii::$app->id
			&& isset(\Yii::$app->params['api']['secrets'][ $service ])
			&& \Yii::$app->params['api']['secrets'][ $service ] === $secret_key
		) {
			return $this->setIssuedAccessKey($service);
		} else {
			$error_code = self::ERROR_ILLEGAL_SECRET_KEY;
		}
		return $this->sendError($error_code);
	}

	/**
	 * @param $service string
	 * @return array
	 */
	protected function setIssuedAccessKey($service) {
		if($model = IssuedAccessKeys::findOne( [ 'service' => $service ] )) {
			$model->access_key = \Yii::$app->security->generateRandomString(64);
			$model->expires_in = time() + \Yii::$app->params['api']['expires_on'];
		} else {
			$model = new IssuedAccessKeys();
			$model->service = $service;
			$model->access_key = \Yii::$app->security->generateRandomString(64);
			$model->expires_in = time() + \Yii::$app->params['api']['expires_on'];
		}
		if($model->save()) {
			return $this->sendSuccess([
				'access_key' => $model->access_key,
				'expires_in' => $model->expires_in,
			]);
		} else {
			return $this->sendError(self::ERROR_DB);
		}
	}
}