<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\widgets;


class MonoMenu extends \yii\widgets\Menu {
	public $items;
	public function run() {
		$links = [];
		foreach($this->items as $link) {
			$links[ $link->number ] = [
				'label' => $link->name,
				'url' => json_decode($link->route, true),
			];

		}
		if($links) {
			ksort($links);
			$this->items = array_values($links);
		}

		return parent::run();
	}
} 